package com.ucbcba.logindemo.entities;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Recordatorio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    @NotNull
    private String observaciones="Sin Observaciones";

    @NotNull
    private String hora = "";

    @NotNull
    private String medicamento="";

    @NotNull
    private String dosis="";

    @NotNull
    private Integer repetir=0;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getMedicamento() {
        return medicamento;
    }

    public void setMedicamento(String medicamento) {
        this.medicamento = medicamento;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public Integer getRepetir() {
        return repetir;
    }

    public void setRepetir(Integer repetir) {
        this.repetir = repetir;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}

