package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Tratamiento;

public interface TratamientoService {
    Iterable<Tratamiento>listAllTratamientos();
    void saveTratamiento(Tratamiento tratamiento);
    void deleteTratamiento (Integer id);
    Tratamiento getTratamiento(Integer id);
}
