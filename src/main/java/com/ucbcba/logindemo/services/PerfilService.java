package com.ucbcba.logindemo.services;


import com.ucbcba.logindemo.entities.Perfil;

public interface PerfilService {

    Iterable<Perfil> listAllProfiles();
    void saveProfile(Perfil perfil);
    Perfil getProfile(Integer id);
    void deleteProfile(Integer id);

}
