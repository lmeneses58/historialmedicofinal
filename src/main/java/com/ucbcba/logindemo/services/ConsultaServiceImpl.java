package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Consulta;
import com.ucbcba.logindemo.entities.Perfil;
import com.ucbcba.logindemo.repositories.ConsultaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ConsultaServiceImpl implements ConsultaService {

    @Autowired
    private ConsultaRepository consultaRepository;

    @Autowired
    @Qualifier(value = "consultaRepository")
    public void setConsultaRepository(ConsultaRepository consultaRepository) {
        this.consultaRepository = consultaRepository;
    }

    @Override
    public Iterable<Consulta> listAllCons() {
        return consultaRepository.findAll();
    }

    @Override
    public void saveCons(Consulta consulta) {
        consultaRepository.save(consulta);
    }

    @Override
    public Consulta getCons(Integer id) {
        return consultaRepository.findById(id).get();
    }

    @Override
    public void deleteCons(Integer id) {
        consultaRepository.deleteById(id);
    }

    @Override
    public Iterable<Consulta> filterByGravedad(String query) {
        return consultaRepository.filterByGravedad(query);
    }

    @Override
    public Iterable<Consulta> searchByRangeDates(Date query1, Date query2){
        return consultaRepository.searchByRangeDates(query1, query2);
    }



}
