package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Recordatorio;
import com.ucbcba.logindemo.repositories.RecordatorioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class RecordatorioServiceImpl implements RecordatorioService {

    private RecordatorioRepository recordatorioRepository;

    @Autowired
    @Qualifier(value = "recordatorioRepository")
    public void setRecordatorioRepository(RecordatorioRepository recordatorioRepository) {
        this.recordatorioRepository = recordatorioRepository;
    }

    @Override
    public Iterable<Recordatorio> listAllRecordatorios() {
        return recordatorioRepository.findAll();
    }

    @Override
    public void saveRecordatorio(Recordatorio recordatorio) {
        recordatorioRepository.save(recordatorio);
    }

    @Override
    public Recordatorio getRecordatorio(Integer id) {
        return recordatorioRepository.findById(id).get();
    }

    @Override
    public void deleteRecordatorio(Integer id) {
        recordatorioRepository.deleteById(id);
    }
}
