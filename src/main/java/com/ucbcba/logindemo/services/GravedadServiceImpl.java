package com.ucbcba.logindemo.services;
import com.ucbcba.logindemo.entities.Gravedad;
import com.ucbcba.logindemo.repositories.GravedadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GravedadServiceImpl implements GravedadService{

    @Autowired
    private GravedadRepository gravedadRepository;

    @Autowired
    @Qualifier(value = "gravedadRepository")
    public void setGravedadRepository(GravedadRepository gravedadRepository) {
        this.gravedadRepository= gravedadRepository;
    }

    @Override
    public Iterable<Gravedad> listAllGravedads() {
        return gravedadRepository.findAll();
    }

    @Override
    public void saveGravedad(Gravedad gravedad) {
        gravedadRepository.save(gravedad);
    }

    @Override
    public Gravedad getGravedad(Integer id) {
        return gravedadRepository.findById(id).get();
    }

    @Override
    public void deleteGravedad(Integer id) {
        gravedadRepository.deleteById(id);

    }
}

