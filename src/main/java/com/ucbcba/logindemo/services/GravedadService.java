package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Gravedad;

public interface GravedadService {

    Iterable<Gravedad> listAllGravedads();

    void saveGravedad(Gravedad gravedad);

    Gravedad getGravedad(Integer id);

    void deleteGravedad(Integer id);
}

