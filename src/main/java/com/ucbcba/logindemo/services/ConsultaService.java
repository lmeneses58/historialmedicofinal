package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Consulta;
import com.ucbcba.logindemo.entities.Perfil;

import java.util.Date;

public interface ConsultaService {

    Iterable<Consulta> listAllCons();
    void saveCons(Consulta consulta);
    Consulta getCons(Integer id);
    void deleteCons(Integer id);
    Iterable<Consulta>filterByGravedad(String query);
    Iterable<Consulta>searchByRangeDates(Date query1, Date query2);

}
