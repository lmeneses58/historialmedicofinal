package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Perfil;
import com.ucbcba.logindemo.repositories.PerfilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PerfilServiceImpl implements PerfilService {

    @Autowired
    @Qualifier(value = "perfilRepository")
    private PerfilRepository perfilRepository;

    public void setPerfilRepository(PerfilRepository perfilRepository) {
        this.perfilRepository = perfilRepository;
    }

    @Override
    public Iterable<Perfil> listAllProfiles() {
        return perfilRepository.findAll();
    }

    @Override
    public void saveProfile(Perfil perfil) {
        perfilRepository.save(perfil);
    }

    @Override
    public Perfil getProfile(Integer id) {
        return perfilRepository.findById(id).get();
    }

    @Override
    public void deleteProfile(Integer id) {
        perfilRepository.deleteById(id);
    }
}
