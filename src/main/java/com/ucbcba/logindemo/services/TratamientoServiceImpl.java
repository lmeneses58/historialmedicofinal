package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Tratamiento;
import com.ucbcba.logindemo.repositories.TratamientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class TratamientoServiceImpl implements TratamientoService{

    @Autowired
    private TratamientoRepository tratamientoRepository;

    public void setTratamientoRepository(TratamientoRepository tratamientoRepository) {
        this.tratamientoRepository = tratamientoRepository;
    }

    @Override
    public Iterable<Tratamiento> listAllTratamientos() {
        return tratamientoRepository.findAll();
    }

    @Override
    public void saveTratamiento(Tratamiento tratamiento) {
        tratamientoRepository.save(tratamiento);
    }

    @Override
    public void deleteTratamiento(Integer id) {
        tratamientoRepository.deleteById(id);
    }

    @Override
    public Tratamiento getTratamiento(Integer id) {
        return tratamientoRepository.findById(id).get();
    }
}
