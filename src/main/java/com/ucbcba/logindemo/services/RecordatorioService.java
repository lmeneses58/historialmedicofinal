package com.ucbcba.logindemo.services;

import com.ucbcba.logindemo.entities.Recordatorio;

public interface RecordatorioService {

    Iterable<Recordatorio> listAllRecordatorios();

    void saveRecordatorio(Recordatorio recordatorio);

    Recordatorio getRecordatorio(Integer id);

    void deleteRecordatorio(Integer id);
}
