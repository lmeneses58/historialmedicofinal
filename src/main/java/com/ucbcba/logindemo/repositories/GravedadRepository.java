package com.ucbcba.logindemo.repositories;

import com.ucbcba.logindemo.entities.Gravedad;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface GravedadRepository extends CrudRepository<Gravedad, Integer> {


}
