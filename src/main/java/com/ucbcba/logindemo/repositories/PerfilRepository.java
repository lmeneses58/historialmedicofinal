package com.ucbcba.logindemo.repositories;

import com.ucbcba.logindemo.entities.Perfil;
import com.ucbcba.logindemo.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface PerfilRepository extends CrudRepository <Perfil, Integer> {
}
