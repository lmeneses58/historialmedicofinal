package com.ucbcba.logindemo.repositories;

import com.ucbcba.logindemo.entities.Tratamiento;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface TratamientoRepository extends CrudRepository<Tratamiento,Integer> {
}
