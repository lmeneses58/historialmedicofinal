package com.ucbcba.logindemo.repositories;

import com.ucbcba.logindemo.entities.Recordatorio;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface RecordatorioRepository extends CrudRepository<Recordatorio, Integer> {
}
