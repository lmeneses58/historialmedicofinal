package com.ucbcba.logindemo.repositories;

import com.ucbcba.logindemo.entities.Consulta;
import com.ucbcba.logindemo.entities.Perfil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Date;


@Transactional
public interface ConsultaRepository extends CrudRepository<Consulta, Integer> {

    @Query("SELECT b FROM Consulta b WHERE b.date LIKE %:date%")
    Iterable<Consulta> findByDate(@Param("date") Date date);

    @Query("select m from Consulta m where m.gravedad like CONCAT('%',:gravedad,'%')")
    public Iterable<Consulta>filterByGravedad(@Param("gravedad") String gravedad);

    @Query("select m from Consulta m where m.date >= :dateini AND m.date <= :datefin")
    public Iterable<Consulta>searchByRangeDates(@Param("dateini") Date dateini, @Param("datefin") Date datefin);


}

