package com.ucbcba.logindemo.controllers;

import com.ucbcba.logindemo.entities.Perfil;
import com.ucbcba.logindemo.entities.User;
import com.ucbcba.logindemo.services.PerfilService;
import com.ucbcba.logindemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class PerfilController {


    private PerfilService perfilService;
    private UserService userService;
    private String username;

    @Autowired
    public void setPerfilService(PerfilService perfilService) {
        this.perfilService = perfilService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(value = "/nuevoPerfil", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("perfil", new Perfil());
        return "formularioPerfil";
    }

    @RequestMapping(value = "/guardarPerfil", method = RequestMethod.POST)
    String savePerfil(Model model,@ModelAttribute("perfil") Perfil perfil) {
       String username =  this.username = SecurityContextHolder.getContext().getAuthentication().getName();

        com.ucbcba.logindemo.entities.User user1 = userService.findByUsername(username);
        perfil.setUser_id(user1.getId());
        perfilService.saveProfile(perfil);
        model.addAttribute("perfil", perfil);
        return "redirect:/consulta";
    }

    @RequestMapping("/modificarPerfil/{id}")
    String modificarPerfil(@PathVariable Integer id, Model model) {
        model.addAttribute("perfil", perfilService.getProfile(id));
        return "modificarPerfil";
    }

    @RequestMapping("/eliminarPerfil/{id}")
    String eliminar(@PathVariable Integer id) {
        perfilService.deleteProfile(id);
        return "redirect:/consulta";
    }

    @RequestMapping(value = "/verPerfil/{id}", method = RequestMethod.GET)
    public String verPerfil(Model model, @PathVariable Integer id) {
        model.addAttribute("perfil", perfilService.getProfile(id));
        return "verPerfil";
    }
}
