package com.ucbcba.logindemo.controllers;

import com.ucbcba.logindemo.entities.Tratamiento;
import com.ucbcba.logindemo.services.TratamientoService;
import com.ucbcba.logindemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TratamientoController {

    private TratamientoService tratamientoService;
    private UserService userService;

    @Autowired
    public void setUserService (UserService userService){this.userService=userService;}

    @Autowired
    public void setTratamientoService(TratamientoService tratamientoService) {
        this.tratamientoService = tratamientoService;
    }

    @RequestMapping(value = "/tratamientos", method = RequestMethod.GET)
    public String list(Model model) {
        Iterable<Tratamiento> listaTratamientos = tratamientoService.listAllTratamientos();
        model.addAttribute("listaTratamientos", listaTratamientos);
        return "mostrarTratamientos";
    }

    @RequestMapping("/nuevoTratamiento")
    String nuevoTratamiento(Model model) {
        model.addAttribute("tratamiento", new Tratamiento());
        return "formularioTratamiento";
    }

    @RequestMapping(value = "/guardarTratamiento", method = RequestMethod.POST)
    String saveTratamiento(@ModelAttribute("tratamiento") Tratamiento tratamiento) {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        com.ucbcba.logindemo.entities.User user1 =userService.findByUsername(username);

        tratamiento.setUser(user1);
        tratamientoService.saveTratamiento(tratamiento);
        return "redirect:/consulta";
    }

    @RequestMapping("/modificarTratammiento/{id}")
    String modificarTratamiento(@PathVariable Integer id, Model model) {
        model.addAttribute("tratamiento", tratamientoService.getTratamiento(id));
        return "modificarTratammiento";
    }

    @RequestMapping("/eliminarTratamiento/{id}")
    String eliminarTratamiento(@PathVariable Integer id) {
        tratamientoService.deleteTratamiento(id);
        return "redirect:/consulta";
    };

    @RequestMapping(value = "/verTratamiento/{id}", method = RequestMethod.GET)
    public String verTratamiento(Model model, @PathVariable Integer id) {
        model.addAttribute("tratamiento", tratamientoService.getTratamiento(id));
        return "verTratamiento";
    }
}
