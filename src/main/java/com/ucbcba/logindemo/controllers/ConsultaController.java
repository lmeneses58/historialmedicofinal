package com.ucbcba.logindemo.controllers;

import com.ucbcba.logindemo.entities.Consulta;
import com.ucbcba.logindemo.entities.Perfil;
import com.ucbcba.logindemo.entities.Recordatorio;
import com.ucbcba.logindemo.entities.Tratamiento;
import com.ucbcba.logindemo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
public class ConsultaController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String registrationInit() {
        return "login";
    }
    @Autowired
    private ConsultaService consultaService;

    @Autowired
    private PerfilService perfilService;

    @Autowired
    private GravedadService gravedadService;

    @Autowired
    private UserService userService;

    @Autowired
    private TratamientoService tratamientoService;

    @Autowired
    private RecordatorioService recordatorioService;

    private String username;


    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    public void setPerfilService(PerfilService perfilService) {
        this.perfilService = perfilService;
    }

    public void setGravedadService(GravedadService gravedadService) {
        this.gravedadService = gravedadService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTratamientoService (TratamientoService tratamientoService) {this.tratamientoService=tratamientoService;}

    public void setRecordatorioService (RecordatorioService recordatorioService) {this.recordatorioService = recordatorioService;}


    @RequestMapping(value = "/consulta", method = RequestMethod.GET)
    public String index(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();

        com.ucbcba.logindemo.entities.User user1 = userService.findByUsername(username);
        model.addAttribute("user", user1);

        List<Consulta> consulta = (List) consultaService.listAllCons();
        ((ArrayList<Consulta>) consulta).sort((d1, d2) -> d1.getDate().compareTo(d2.getDate()));
        Collections.reverse(((ArrayList<Consulta>) consulta));
        List<Tratamiento> tratamiento = (List) tratamientoService.listAllTratamientos();
        List<Perfil> perfil = (List) perfilService.listAllProfiles();
        List<Recordatorio> recordatorio = (List) recordatorioService.listAllRecordatorios();

        model.addAttribute("consulta",consulta);
        model.addAttribute("perfil",perfil);
        model.addAttribute("tratamiento",tratamiento);
        model.addAttribute("recordatorio",recordatorio);
        return "/consulta";
    }


    @RequestMapping (value = "/consulta/crearConsulta", method = RequestMethod.GET)
    public String crearConsulta(Model model) {

        model.addAttribute("consulta",new Consulta());
        model.addAttribute("gravedad", gravedadService.listAllGravedads());
        return "crearConsulta";
    }


    @RequestMapping (value = "/consulta", method = RequestMethod.POST)
    public String create(@ModelAttribute("consulta") Consulta consulta, Model model)
    {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();
        com.ucbcba.logindemo.entities.User user1 =userService.findByUsername(username);
        consulta.setUser(user1);
        consultaService.saveCons(consulta);
        return "redirect:/consulta";
    }

    @RequestMapping(value = "/consulta/delete/{id}",method = RequestMethod.GET )
    public String delete (@PathVariable Integer id, Model model)
    {
        consultaService.deleteCons(id);
        return "redirect:/consulta";
    }

    @RequestMapping(value = "/consulta/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model) {
        Consulta consulta = consultaService.getCons(id);
        model.addAttribute("consulta", consulta);
        model.addAttribute("gravedad", gravedadService.listAllGravedads());
        return "editConsulta";
    }

    // FILTRAR GRAVEDAD
    @RequestMapping(value = "/searchGravedad/", method = RequestMethod.GET)
    public String searchA(@RequestParam("query") String query, Model model) {

        String username =  this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        com.ucbcba.logindemo.entities.User user1 = userService.findByUsername(username);
        model.addAttribute("user", user1);

        List<Consulta> consultas = (List) consultaService.filterByGravedad(query);
        ArrayList<Consulta> consultas1= new ArrayList<>();
        for (int i = 0; i < consultas.size(); i++) {
            if(user1.getId() == consultas.get(i).getUser().getId()){
                consultas1.add(consultas.get(i));
            }
        }
        model.addAttribute("consulta", consultas1);
        return "filter";
    }

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filter(Model model) {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();

        com.ucbcba.logindemo.entities.User user1 = userService.findByUsername(username);
        model.addAttribute("user", user1);

        ArrayList<Consulta> consultas = (ArrayList) consultaService.listAllCons();
        ArrayList<Consulta> medaux= new ArrayList<>();
        for (int i = 0; i < consultas.size(); i++) {
            if(user1.getId() == consultas.get(i).getUser().getId()){
                medaux.add(consultas.get(i));
            }
        }
        model.addAttribute("consultas", medaux);
        return "filter";
    }

    //BUSCAR POR FECHAS

    @RequestMapping(value = "/searchDate/", method = RequestMethod.GET)
    public String searchDate(@RequestParam("query1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date query1, @RequestParam("query2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date query2 ,Model model) {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();

        com.ucbcba.logindemo.entities.User user1 = userService.findByUsername(username);
        model.addAttribute("user", user1);
        List<Consulta> consultas = (List) consultaService.searchByRangeDates(query1, query2);

        ArrayList<Consulta> consultas1= new ArrayList<>();
        for (int i = 0; i < consultas.size(); i++) {
            if(user1.getId() == consultas.get(i).getUser().getId()){
                consultas1.add(consultas.get(i));
            }
        }
        model.addAttribute("consulta", consultas1);
        return "searchByDates";
    }

    @RequestMapping(value = "/searchByDates", method = RequestMethod.GET)
    public String searchByDates(Model model) {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = user.getUsername();

        com.ucbcba.logindemo.entities.User user1 = userService.findByUsername(username);
        model.addAttribute("user", user1);

        ArrayList<Consulta> consultas = (ArrayList) consultaService.listAllCons();
        ArrayList<Consulta> consultas1= new ArrayList<>();
        for (int i = 0; i < consultas.size(); i++) {
            if(user1.getId() == consultas.get(i).getUser().getId()){
                consultas1.add(consultas.get(i));
            }
        }
        model.addAttribute("consulta", consultas1);
        return "searchByDates";
    }

}