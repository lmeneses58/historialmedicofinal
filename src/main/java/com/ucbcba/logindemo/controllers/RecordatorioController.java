package com.ucbcba.logindemo.controllers;


import com.ucbcba.logindemo.entities.Recordatorio;
import com.ucbcba.logindemo.services.RecordatorioService;
import com.ucbcba.logindemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

@Controller
public class RecordatorioController {

    private RecordatorioService recordatorioService;
    private UserService userService;
    private String username;

    @Autowired
    public void setRecordatorioService(RecordatorioService recordatorioService) {
        this.recordatorioService = recordatorioService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping("/nuevoRecordatorio")
    String nuevoRecordaorioTomaDeMedicamento(Model model) {
        model.addAttribute("listaRecordatorios",  new Recordatorio());
        return "formularioRecordatorio";
    }

    @RequestMapping(value = "/guardarRecordatorio", method = RequestMethod.POST)
    String save(Recordatorio recordatorio, RedirectAttributes redirectAttributes) throws IOException {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        this.username = user.getUsername();
        com.ucbcba.logindemo.entities.User user1 =userService.findByUsername(username);
        recordatorio.setUser(user1);
        recordatorioService.saveRecordatorio(recordatorio);
        return "redirect:/consulta";
    }

    @RequestMapping("/Recordatorio/{id}")
    String show(@PathVariable Integer id, Model model) {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        this.username = user.getUsername();
        com.ucbcba.logindemo.entities.User user1 =userService.findByUsername(username);
        model.addAttribute("listaRecordatorios", recordatorioService.listAllRecordatorios());
        Recordatorio recordatorio= recordatorioService.getRecordatorio(id);
        model.addAttribute("recordatorio", recordatorio);
        return "verRecordatorio";
    }

    @RequestMapping("/modificarRecordatorio/{id}")
    String modificarRecordatorio(@PathVariable Integer id, Model model) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatorios", recordatorioService.listAllRecordatorios());
        Recordatorio recordatorio= recordatorioService.getRecordatorio(id);
        model.addAttribute("recordatorio", recordatorio);
        return  "modificarRecordatorio";
    }
    @RequestMapping("/eliminarRecordatorio/{id}")
    String eliminar(@PathVariable Integer id) {
        recordatorioService.deleteRecordatorio(id);
        return "redirect:/consulta";
    }
    @RequestMapping(value = "/Recordatorios", method = RequestMethod.GET)
    public String todosrecordatorios(Model model) {
        model.addAttribute("listaRecordatorios", recordatorioService.listAllRecordatorios());
        return "mostrarTodosRecordatorios";
    }


}

